

const target = 'http://192.168.1.35:8080/ydrobot-photovoltaic-api'
// const target = 'http://192.168.1.158:8080/ydrobot-photovoltaic-api'

module.exports = {
  // 部署应用时的基本 URL
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  // build时构建文件的目录 构建时传入 --no-clean 可关闭该行为
  outputDir: 'dist',
  // build时放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录
  assetsDir: './',
  // 指定生成的 index.html 的输出路径 (相对于 outputDir)。也可以是一个绝对路径。
  indexPath: 'index.html',
  // 默认在生成的静态资源文件名中包含hash以控制缓存
  filenameHashing: true,
  // 是否在开发环境下通过 eslint-loader 在每次保存时 lint 代码 (在生产构建时禁用 eslint-loader)
  lintOnSave: process.env.NODE_ENV !== 'production',
  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: true,
  // Babel 显式转译列表
  transpileDependencies: [],
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建
  productionSourceMap: true,
  // 设置生成的 HTML 中 <link rel="stylesheet"> 和 <script> 标签的 crossorigin 属性（注：仅影响构建时注入的标签）

  crossorigin: '',
  // 在生成的 HTML 中的 <link rel="stylesheet"> 和 <script> 标签上启用 Subresource Integrity (SRI)
  integrity: false,
  // 对内部的 webpack 配置（比如修改、增加Loader选项）(链式操作)
  chainWebpack: () => {},
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          //   require('autoprefixer')({
          //     // 配置使用 autoprefixer
          //     overrideBrowserslist: ['last 15 versions']
          //   }),
          require('postcss-pxtorem')({
            rootValue: 100, // 换算的基数
            // 忽略转换正则匹配项。插件会转化所有的样式的px。比如引入了三方UI，也会被转化。目前我使用 selectorBlackList字段，来过滤
            //如果个别地方不想转化px。可以简单的使用大写的 PX 或 Px 。
            selectorBlackList: ['norem'],
            propList: ['*']
            // exclude: /node_modules/
          })
        ]
      }
    }
  },
  configureWebpack: config => {
    // 不是开发环境时生效
    if (process.env.NODE_ENV !== 'development') {
      config['performance'] = {
        // 警告 webpack 的性能提示
        hints: 'warning',
        // 入口起点的最大体积
        maxEntrypointSize: 50000000,
        // 生成文件的最大体积
        maxAssetSize: 30000000,
        // 只给出 js 文件的性能提示
        assetFilter: function (assetFilename) {
          return assetFilename.endsWith('.js')
        }
      }
    }
  },

  devServer: {
    proxy: {
      '/ydrobot-photovoltaic-api': {
        target: target,
        changeOrigin: true,
        pathRewrite: {
          '^/ydrobot-photovoltaic-api': ''
        }
      },
      '/photovoltaic': {
        target: target,
        changeOrigin: true,
        pathRewrite: {
          '^/photovoltaic': 'photovoltaic'
        }
      }
    },
    before: () => {}
  },
  // 是否为 Babel 或 TypeScript 使用 thread-loader
  parallel: require('os').cpus().length > 1,
  // 向 PWA 插件传递选项
  pwa: {},
  // 可以用来传递任何第三方插件选项
  pluginOptions: {},

  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      title: '新能源光伏智能运维系统'
    }
  }
}
