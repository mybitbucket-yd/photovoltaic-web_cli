import { RouteRecordRaw } from 'vue-router'
import Layout from '@/layout/container.vue'
import Springboard from '@/layout/springboard.vue'
import Login from '@/views/login/login.vue'

// const viewsCompon = import.meta.glob('../views/**')

// console.log(viewsCompon, 'viewsCompon')
// let arrVar: any = []
// const arr = Object.keys(viewsCompon).map(item => {
//   let key = item.match(/views\/(\S*)\.vue/)
//   let keyArr = key[1].split('/')
//   let obj = {
//     path: '/' + key[1],
//     name: keyArr[keyArr.length - 1],
//     component: () => import(item),
//     meta: {
//       title: '',
//       pname: keyArr[keyArr.length - 2] ? keyArr[keyArr.length - 2] : null
//     }
//   }
//   return obj
// })
// console.log(arr, '11111111111111')

// let eee = []
// arr.forEach(item => {
//   let keysa = item.path.split('/')
//   let keys = keysa.slice(1, keysa.length - 1)
//   console.log(keys, 'key============')
//   for (let i = 0; i < keys.length; i++) {
//     let obj = {
//       path: '/' + keys[i],
//       name: keys[i],
//       component: Springboard,
//       meta: {
//         title: '',
//         pname: keys[i - 1] ? keys[i - 1] : null
//       },
//       children: []
//     }
//     let bol = eee.some(t => {
//       return t.name == keys[i]
//     })
//     if (!bol) {
//       eee.push(obj)
//     }
//   }
// })
// console.log(eee, '4444444444455555555')

export const layoutChildren: Array<RouteRecordRaw> = [
  //   {
  //     path: '/home',
  //     name: 'home',
  //     component: () => import('@/views/home/home.vue'),
  //     meta: {
  //       title: 'home'
  //     }
  //   },
  //   首页
  {
    path: '/index',
    name: 'index',
    // redirect: '/index/integrated',
    component: Springboard,
    meta: {
      title: '首页'
    },
    children: [
      {
        path: '/index/integrated',
        name: 'integrated',
        component: () => import('../views/index/integrated.vue'),
        meta: {
          title: '综合大屏'
          //   noSider: true
        }
      },
      {
        path: '/index/controlCenter',
        name: 'controlCenter',
        component: () => import('@/views/index/controlCenter.vue'),
        meta: {
          title: '集控中心'
        }
      },
      {
        path: '/index/siteOverview',
        name: 'siteOverview',
        component: () => import('@/views/index/siteOverview.vue'),
        meta: {
          title: '站点概览'
        }
      }
    ]
  },
  {
    path: '/realTimeMonitor',
    name: 'realTimeMonitor',
    component: Springboard,
    redirect: '/realTimeMonitor/inverter/overview',
    meta: {
      title: '实时监测'
    },
    children: [
      {
        path: '/realTimeMonitor/inverter',
        name: 'inverter',
        redirect: '/realTimeMonitor/inverter/overview',
        component: Springboard,
        meta: {
          title: '逆变器'
        },
        children: [
          {
            path: '/realTimeMonitor/inverter/overview',
            name: 'overview',
            component: () => import('@/views/realTimeMonitor/inverter/overview.vue'),
            meta: {
              title: '总览'
            }
          },
          {
            path: '/realTimeMonitor/inverter/monitorInfo',
            name: 'monitorInfo',
            component: () => import('@/views/realTimeMonitor/inverter/monitorInfo.vue'),
            meta: {
              title: '监测信息'
            }
          },
          {
            path: '/realTimeMonitor/inverter/viewByStatus',
            name: 'viewByStatus',
            component: () => import('@/views/realTimeMonitor/inverter/viewByStatus.vue'),
            meta: {
              title: '按状态查看'
            }
          }
        ]
      },
      {
        path: '/realTimeMonitor/WattHourMeter',
        name: 'WattHourMeter',
        component: () => import('@/views/realTimeMonitor/WattHourMeter.vue'),
        meta: {
          title: '电度表'
        }
      },
      {
        path: '/realTimeMonitor/combinerBox',
        name: 'combinerBox',
        component: () => import('@/views/realTimeMonitor/combinerBox.vue'),
        meta: {
          title: '汇流箱'
        }
      },
      {
        path: '/realTimeMonitor/historicalAlarm',
        name: 'historicalAlarm',
        component: () => import('@/views/realTimeMonitor/historicalAlarm.vue'),
        meta: {
          title: '历史告警'
        }
      }
    ]
  },
  {
    path: '/alarmAnalysis',
    name: 'alarmAnalysis',
    component: () => import('@/views/alarmAnalysis/alarmAnalysis.vue'),
    meta: {
      title: '报警分析'
    }
  },
  {
    path: '/equipmentAnalysis',
    name: 'equipmentAnalysis',
    component: Springboard,
    redirect: '/equipmentAnalysis/inverterAnalysis',
    meta: {
      title: '设备分析'
    },
    children: [
      {
        path: '/equipmentAnalysis/inverterAnalysis',
        name: 'inverterAnalysis',
        component: () => import('@/views/equipmentAnalysis/inverterAnalysis.vue'),
        meta: {
          title: '逆变器分析'
        }
      },
      {
        path: '/equipmentAnalysis/wattHourAnalysis',
        name: 'wattHourAnalysis',
        component: () => import('../views/equipmentAnalysis/wattHourAnalysis.vue'),
        meta: {
          title: '电度表分析'
        }
      }
    ]
  },
  {
    path: '/statisticalReport',
    name: 'statisticalReport',
    component: Springboard,
    redirect: '/statisticalReport/powerGeneration',
    meta: {
      title: '统计报表'
    },
    children: [
      {
        path: '/statisticalReport/powerGeneration',
        name: 'powerGeneration',
        component: () => import('@/views/statisticalReport/powerGeneration.vue'),
        meta: {
          title: '发电量统计'
        }
      },
      {
        path: '/statisticalReport/loadStatistics',
        name: 'loadStatistics',
        component: () => import('../views/statisticalReport/loadStatistics.vue'),
        meta: {
          title: '负荷统计'
        }
      },
      {
        path: '/statisticalReport/siteDaily',
        name: 'siteDaily',
        component: () => import('../views/statisticalReport/siteDaily.vue'),
        meta: {
          title: '站点日统计'
        }
      }
    ]
  },
  {
    path: '/fileManage',
    name: 'fileManage',
    component: Springboard,
    redirect: '/fileManage/siteArchives',
    meta: {
      title: '档案管理'
    },
    children: [
      {
        path: '/fileManage/siteArchives',
        name: 'siteArchives',
        component: () => import('@/views/fileManage/siteArchives.vue'),
        meta: {
          title: '站点档案'
        }
      },
      {
        path: '/fileManage/enterpriseArchives',
        name: 'enterpriseArchives',
        component: () => import('@/views/fileManage/enterpriseArchives.vue'),
        meta: {
          title: '企业档案概览'
        }
      },
      {
        path: '/fileManage/enterprisedtails',
        name: 'enterprisedtails',
        component: () => import('@/views/fileManage/enterprisedtails.vue'),
        meta: {
          title: '企业档案详情'
        }
      },
      {
        path: '/fileManage/equipmentArchives',
        name: 'equipmentArchives',
        component: () => import('@/views/fileManage/equipmentArchives.vue'),
        meta: {
          title: '设备档案'
        }
      }
    ]
  },
  {
    path: '/mochaITOM',
    name: 'mochaITOM',
    component: () => import('@/views/mochaITOM/mochaITOM.vue'),
    meta: {
      title: '运维管理'
    }
  },
  {
    path: '/gateWayAdmin',
    name: 'gateWayAdmin',
    component: () => import('@/views/gateWayAdmin/gateWayAdmin.vue'),
    meta: {
      title: '网关管理'
    }
  },
  {
    path: '/sysManage',
    name: 'sysManage',
    component: Springboard,
    redirect: '/sysManage/paramConfig',
    meta: {
      title: '系统管理'
    },
    children: [
      {
        path: '/sysManage/paramConfig',
        name: 'paramConfig',
        component: () => import('@/views/sysManage/paramConfig.vue'),
        meta: {
          title: '参数配置'
        }
      }
    ]
  }
]

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    name: 'layout',
    component: Layout,
    redirect: '/index/siteOverview',
    children: layoutChildren
  }
]

export default routes
