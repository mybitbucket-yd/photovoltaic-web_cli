// @ts-nocheck
import * as elementResizeDetector from 'element-resize-detector'

export function selfAdaption(res: number) {
  let clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
  if (!clientWidth) return
  let fontSize = clientWidth / 1920
  return res * fontSize
}

export function dateFormat(date: any, istime?: boolean) {
  let y = date.getFullYear()
  let m = date.getMonth() + 1
  m = m < 10 ? '0' + m : m
  var d = date.getDate()
  d = d < 10 ? '0' + d : d
  var h = date.getHours()
  h = h < 10 ? '0' + h : h
  var minute = date.getMinutes()
  minute = minute < 10 ? '0' + minute : minute
  var second = date.getSeconds()
  second = second < 10 ? '0' + second : second
  if (istime) return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second
  return y + '-' + m + '-' + d
}

export function cgDateTime(dateTime) {
  console.log(dateTime, 'dateTime')
  let reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/
  if (!dateTime || dateTime == '') {
    return dateFormat(new Date(), true)
  }
  if (dateTime.toString().match(reg)) {
    return dateTime
  }
  if (dateTime) {
    return dateFormat(dateTime, true)
  }
}

let erd = elementResizeDetector()
export function resize(el: any, id: string) {
  //   console.log(el, id, '999999')
  erd.listenTo(document.getElementById(id), function (e) {
    el.resize()
  })
}




// 1->隐患；2->一般告警；3->严重告警；4->危急告警
export const alarmLeveArr = [
  { id: 1, name: '隐患' },
  { id: 2, name: '一般告警' },
  { id: 3, name: '严重告警' },
  { id: 4, name: '危急告警' }
]
export function getAlarmLevelName(id) {
  let name = ''
  alarmLeveArr.forEach(item => {
    if (item.id == id) {
      name = item.name
    }
  })
  return name
}

// getDay(0);//当天日期
// getDay(-7);//7天前日期
// getDay(-3);//3天前日期
export function getDay(day) {
  var today = new Date()
  var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day
  today.setTime(targetday_milliseconds) //注意，这行是关键代码
  var tYear = today.getFullYear()
  var tMonth = today.getMonth()
  var tDate = today.getDate()
  tMonth = doHandleMonth(tMonth + 1)
  tDate = doHandleMonth(tDate)
  return tYear + '-' + tMonth + '-' + tDate
}
function doHandleMonth(month) {
  var m = month
  if (month.toString().length == 1) {
    m = '0' + month
  }
  return m
}

//获得本周、上周的开始结束时间
export function getWeekDate(n = 0) {
  //   //上周的开始时间
  //   console.log(getWeekDate(7))
  //   //上周的结束时间
  //   console.log(getWeekDate(1))
  //   //本周的开始时间
  //   console.log(getWeekDate(0))
  //   //本周的结束时间
  //   console.log(getWeekDate(-6))
  //   //下周的开始时间
  //   console.log(getWeekDate(-7))
  //   //下周的结束时间
  //   console.log(getWeekDate(-13))
  let now = new Date()
  let year = now.getFullYear()
  let month = now.getMonth() + 1
  let day = now.getDay() //返回星期几的某一天;
  n = day == 0 ? n + 6 : n + (day - 1)
  now.setDate(now.getDate() - n)
  let date = now.getDate()
  let s = year + '-' + (month < 10 ? '0' + month : month) + '-' + (date < 10 ? '0' + date : date)
  return s
}
export const Base64 = {
  //加密
  encode(str) {
    return btoa(
      encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
        return String.fromCharCode('0x' + p1)
      })
    )
  },
  //解密
  decode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(
      atob(str)
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
        })
        .join('')
    )
  }
}

export function getStatusColor(status) {
  let color = ''
  switch (status) {
    case 1001:
      color = '#30E0A1'
      break
    case 1002:
      color = 'rgba(255, 149, 59, 1)'
      break
    case 1003:
      color = 'rgb(255, 52, 101)'
      break
    case 1004:
      color = 'rgba(46, 224, 162,0.5)'
      break
    case 1005:
      color = '#ccc'
      break
    case 1006:
      color = '#34f8ff'
      break
    default:
      break
  }
  return color
}
