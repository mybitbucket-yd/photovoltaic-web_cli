import { createStore } from 'vuex'

import moduleStore from './modules/moduleStore'

export default createStore({
  modules: {
    moduleStore
  }
})