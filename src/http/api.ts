// @ts-nocheck
import { GET, postFrom, postJson, DELETE, PUT } from './http'

// http://192.168.1.158:8080/ydrobot-photovoltaic-api/swagger-ui.html

const url = process.env.NODE_ENV == 'development' ? '/ydrobot-photovoltaic-api' : ''

// 企业档案
export const getCompanys = params => GET(url + '/companys', params)
export const addCompanys = params => postJson(url + '/companys', params)
export const updateCompanys = (id, params) => PUT(url + '/companys/' + id, params)
export const deleteCompanys = params => DELETE(url + '/companys/{id}?ids=' + params)

// 设备档案
export const getEquipments = params => GET(url + '/equipments', params)
export const addEquipments = params => postJson(url + '/equipments', params)
export const updateEquipments = (id, params) => PUT(url + '/equipments/' + id, params)
export const deleteEquipments = params => DELETE(url + '/equipments/{id}?ids=' + params)
export const getEqmTree = params => GET(url + '/equipments/tree', params)
export const getEquiDetails = id => GET(url + '/equipments/' + id)
export const getEquiDataPoints = id => GET(url + '/equipments/dataPoints?equipmentId=' + id)
export const getEquiOverview = params => GET(url + '/equipments/overview', params)

// 设备类型
export const getEquipTypes = () => GET(url + '/equipmentTypes')

// 设备状态
export const getEquiStatus = params => GET(url + '/equipmentStatus', params)

// 站所
export const getStations = params => GET(url + '/stations', params)
export const getAllStations = params => GET(url + '/stations/listAll', params)
export const addstations = params => postJson(url + '/stations', params)
export const updatestations = (id, params) => PUT(url + '/stations/' + id, params)
export const deletestations = params => DELETE(url + '/stations/{id}?ids=' + params)
export const getStatOver = params => GET(url + '/stations/overview', params)

// 厂家模块
export const getManufacturers = params => GET(url + '/manufacturers', params)

// 网关模块
export const getGateways = params => GET(url + '/gateways', params)
export const addGateways = params => postJson(url + '/gateways', params)
export const updateGateways = (id, params) => PUT(url + '/gateways/' + id, params)
export const deleteGateways = params => DELETE(url + '/gateways/{id}?ids=' + params)
export const propertyDown = params => DELETE(url + '/gateways/' + id + '/propertyDown')

// 统计模块
export const getAlarmLevelChart = () => GET(url + '/stats/alarmLevelChart')
export const getAlalarmTypeChart = () => GET(url + '/stats/alarmTypeChart')
export const getComprehensiveInfo = () => GET(url + '/stats/comprehensiveInfo')
export const getInverterStatusChart = () => GET(url + '/stats/inverterStatusChart')
export const getStationStatusChart = () => GET(url + '/stats/stationStatusChart')
export const getPointHistoryChart = params => GET(url + '/stats/pointHistoryChart', params)
export const getEquiOnlineChart = params => GET(url + '/stats/equipmentOnlineChart', params)

// 驱动参数
export const getGriverParams = params => GET(url + '/driverParams', params)

// 数据点
export const getPoints = params => GET(url + '/points', params)
export const addPoints = params => postJson(url + '/points', params)
export const addBatchPoints = params => postJson(url + '/points/batch', params)
export const updatePoints = (id, params) => PUT(url + '/points/' + id, params)
export const deletePoints = params => DELETE(url + '/points/{id}?ids=' + params)

// 告警类型
export const getAllAlarmTypes = params => GET(url + '/alarmTypes/listAll', params)

// 数据点报警设置
export const getPointAlarmSettings = params => GET(url + '/pointAlarmSettings', params)
export const addBatchPointAlarmSettings = params => postJson(url + '/pointAlarmSettings/batch', params)
export const updatePointAlarmSettings = (id, params) => PUT(url + '/pointAlarmSettings/' + id, params)
export const deletePointAlarmSettings = params => DELETE(url + '/pointAlarmSettings/{id}?ids=' + params)

// 数据点报警记录
export const getPointAlarmHistorys = params => GET(url + '/pointAlarmHistorys', params)
export const getAlarmDistributionChart = params => GET(url + '/pointAlarmHistorys/alarmDistributionChart', params)
export const getAlarmalarmLevelChart = params => GET(url + '/pointAlarmHistorys/alarmLevelChart', params)
export const getAlarmhandleStatusChart = params => GET(url + '/pointAlarmHistorys/handleStatusChart', params)

// 指标码
export const getIndexCodesAll = () => GET(url + '/indexCodes/listAll')

// 分析模块

export const getEquiAlarmChart = params => GET(url + '/analysis/equipmentAlarmChart', params)
export const getEquiDistributionChart = params => GET(url + '/analysis/equipmentDistributionChart', params)
export const getEquiInstalledCapacityChart = params => GET(url + '/analysis/equipmentInstalledCapacityChart', params)
export const getEquiEfficiencyChart = params => GET(url + '/analysis/equipmentEfficiencyChart', params)
export const getEquiHealthRankingChart = params => GET(url + '/analysis/equipmentHealthRankingChart', params)
export const getEquiModelInstalledCapacityRatio = params => GET(url + '/analysis/equipmentModelInstalledCapacityRatio', params)
export const getEquiModelRatio = params => GET(url + '/analysis/equipmentModelRatio', params)




// 报表模块
// 日统计报表
export const getDailyStatReport = params => GET(url + '/reports/dailyStatReport', params)
// 发电量统计报表
export const getPowerGenReport = params => GET(url + '/reports/powerGenerationReport', params)
// 负荷统计报表
export const getLoadStatReport = params => GET(url + '/reports/loadStatReport', params)