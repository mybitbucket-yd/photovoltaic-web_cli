<template>
  <div class="wh100">
    <div class="site_left_title w100 flex flex-jb flex-ac">
      <div>
        <span class="pageTitle">{{ state.stationName }}</span>
        <span class="pageSubTitle"> / {{ state.centerName }}</span>
      </div>
      <G-button class="ml15 F14" size="small" @click="toggleHandler">版式切换</G-button>
    </div>
    <div class="site_main flex w100">
      <SiteLeftNoImg v-if="state.toggleBol" :tableData="state.tableData" :stationId="state.stationId" :companyId="state.companyId" :currStation="state.currStation"></SiteLeftNoImg>
      <SiteLeftImg v-else :tableData="state.tableData" :stationId="state.stationId" :companyId="state.companyId" :currStation="state.currStation"></SiteLeftImg>
      <div class="site_right">
        <div class="box site_right_top">
          <p class="box_title box-bg-small-big">运行状态</p>
          <div class="box_main flex oauto">
            <div class="textcenter pointer" v-for="(item, index) in state.righTopList" @click="runStatus(item.path)" :key="index + 777">
              <img :src="item.src" alt="" />
              <span class="cfff F14">{{ item.name }}</span>
            </div>
          </div>
        </div>
        <div class="box site_right_center">
          <p class="box_title box-bg-small-big">监测查看</p>
          <div class="box_main flex">
            <div class="textcenter site_right_center_item pointer" v-for="(item, index) in state.righCenterList" @click="runStatus(item.path)" :key="index + 66">
              <img :src="item.src" alt="" />
              <span class="cfff F14 block">{{ item.name }}</span>
            </div>
          </div>
        </div>
        <div class="box site_right_bottom">
          <p class="box_title box-bg-small">实时告警</p>
          <div class="box_main">
            <Table :data="state.alarmList" :total="state.tableTotal" @currentChange="tableCurrentChange"></Table>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>
<script setup>
import { computed, reactive, ref, watchEffect, watch, nextTick, onMounted, onUnmounted } from 'vue'
import { useRouter, useRoute } from 'vue-router'

import { getStatOver, getAllStations, getEqmTree, getEquiOnlineChart, getStations, getPointAlarmHistorys, getManufacturers, getIndexCodesAll, getPointHistoryChart } from '@/http/api'

import Table from './components/table.vue'
import SiteLeftNoImg from './components/siteLeft_noimg.vue'
import SiteLeftImg from './components/siteLeft_img.vue'

const router = useRouter()
const route = useRoute()
const state = reactive({
  righTopList: [
    { name: '主接线图', src: require('@/assets/images/zjxt.png') },
    { name: '历史报警', src: require('@/assets/images/lsbj.png') },
    { name: '设备档案', src: require('@/assets/images/sbda.png'), path: '/fileManage/equipmentArchives' },
    { name: '设备分析', src: require('@/assets/images/sbfx.png'), path: '/equipmentAnalysis/inverterAnalysis' },
    { name: '站点统计', src: require('@/assets/images/zdtj.png') }
  ],
  righCenterList: [
    { name: '逆变器', src: require('@/assets/images/nbq.png'), path: '/realTimeMonitor/inverter/overview' },
    { name: '汇流箱', src: require('@/assets/images/hlx.png'), path: '/realTimeMonitor/combinerBox' },
    { name: '电度表', src: require('@/assets/images/ddb.png'), path: '/realTimeMonitor/WattHourMeter' }
  ],
  stationId: '',
  stationName: '',
  treelist: [],
  componyName: '',
  centerName: '',
  companyId: '',
  tablePageNum: 1,
  alarmList: [],
  tableTotal: 0,
  currStation: {},
  tableData: [
    { name: '发电量', type: 'powerGeneration', unit: 'kWh', day: 0 },
    { name: '节煤量', type: 'coalSaving', unit: 'Kg' },
    { name: 'CO2减排量', type: 'co2EmissionReduction', unit: 'Kg' },
    { name: '输出功率/㎡', type: 'outputPower' }
  ],
  toggleBol: false
})

getEqmTreeHandler()

if (Object.keys(route.query).length > 0) {
  state.stationId = route.query.id
  getStatOverHandler()
} else {
  getAllStationsHandler()
}
watchEffect(() => {
  state.treelist.forEach(item => {
    // console.log(item, 'asdasdsad')
    if (state.stationId == item.realId) {
      state.companyId = item.realPid
      state.stationName = item.name
      state.componyName = item.pname
      state.centerName = item.firstName
      getStationsHandler()
    }
  })
})
getPointAlarmHistorysHandler()
let interval = setInterval(() => {
  getPointAlarmHistorysHandler()
}, 5000)

onMounted(() => {})

onUnmounted(() => {
  clearInterval(interval)
})
function toggleHandler() {
  state.toggleBol = !state.toggleBol
}
// 站点信息
function getStationsHandler(isfirst) {
  getStations({ pageNum: 1, pageSize: 99999, companyId: state.companyId }).then(res => {
    if (res.data && res.data.list.length > 0) {
      res.data.list.forEach(item => {
        if (state.stationId == item.id) {
          state.currStation = item
        }
      })
    }
  })
}

function getAllStationsHandler() {
  getAllStations().then(res => {
    if (res.data) {
      state.stationId = res.data[0].id
      state.stationName = res.data[0].id
      getStatOverHandler()
    }
  })
}

function tableCurrentChange(val) {
  state.tablePageNum = val
  getPointAlarmHistorysHandler()
}
// 概况一览
function getStatOverHandler() {
  getStatOver({ stationId: state.stationId }).then(res => {
    state.tableData.forEach(item => {
      res.data.forEach(m => {
        item[m.item] = m.data[item.type]
      })
    })
  })
}
// 实时报警
function getPointAlarmHistorysHandler() {
  getPointAlarmHistorys({ pageNum: state.tablePageNum, pageSize: 10 }).then(res => {
    state.alarmList = res.data.list
    state.tableTotal = res.data.total
  })
}
function getEqmTreeHandler() {
  getEqmTree({ level: 100 }).then(res => {
    forTreeData(res.data, 0, null, null)
  })
}
function forTreeData(arr, id, name, firstName) {
  let fname = firstName || ''
  arr.forEach(item => {
    item.realPid = id
    item.pname = name
    item.firstName = firstName
    if (item.level == 100) {
      state.treelist.push(item)
    }
    if (item.level == 1) {
      fname = item.name
    }
    if (item.children) {
      forTreeData(item.children, item.realId, item.name, fname)
    }
  })
}

function runStatus(path) {
  if (path) {
    router.push({ path, query: { stationId: state.stationId } })
  }
}
</script>
<style lang="less" scoped>
.site_main {
  height: calc(100% - 31px);
}

.site_right {
  width: 25%;
  height: calc(100% - 31px);
  padding-top: 20px;
  .site_right_top {
    // height: 218px;
    .box_main {
      padding-bottom: 20px;
    }
    img {
      width: 90px;
      height: 90px;
      margin: 5px 0;
    }
  }
  .site_right_center {
    margin: 20px 0;
    .site_right_center_item {
      padding: 15px;
      margin: 10px;
      border-radius: 8px;
      border: 1px solid rgba(97, 252, 255, 0.2);
      background: rgba(50, 247, 255, 0.1);
      img {
        width: 48px;
        height: 48px;
      }
    }
  }
  .site_right_bottom {
    height: calc(100% - 390px);
  }
}
</style>
